#!/usr/bin/env bash

# DS-RPAS - Daily Shells Library for RPAS
# License:
#  See README.md document in projects page at
#  https://github.com/stroparo/ds-rpas

# ##############################################################################
# Params

# PS1
if [ -n "${BASH_VERSION}" ] ; then
    #export PS1='[\u@\h \t \$?=$? \W]\$ '
    # Highlight user@host:
    export PS1='[\[\e[32m\]\u@\h\[\e[0m\] \t \$?=$? \W]\$ '
elif [[ $0 = *ksh* ]] && [[ ${SHELL} = *ksh ]] ; then
    export PS1='[${USER}@$(hostname) $(date '+%OH:%OM:%OS') \$=$? ${PWD##*/}]\$ '
fi

# ##############################################################################
# Aliases

alias cdb='d "${DS_RPAS_BUILD:-${DS_ENV}}" -A'
alias cdd='d "${DOM}" || d "${DOMPARENT:-${DOM%/*}}"'
alias cdh='d "${DOM}"/data/hmaint.db'
alias cdi='d "${DOM}"/input'
alias cdo='d "${DOM}"/output'
alias cdp='d "${DOM}"/input/processed'

alias binrpas='cd "${RPAS_HOME}/bin" && ls -ACR'

# ##############################################################################
# Functions

_rpas_aliases () {

    export TMPFILE="${DS_HOME}/_rpas_aliases"
    typeset deny_files='[.](awk|c|h|log|makefile|old|original)$'

    > "${TMPFILE}" || return

    ls -1 "${RPAS_HOME}/bin" \
    | grep '[A-Z]' \
    | grep -i '^[a-z_][a-z_]*$' \
    | egrep -i -v "${deny_files}" \
    | while read p
    do
        echo "alias $(echo "$p" | tr '[:upper:]' '[:lower:]')='${p}'"
    done >> "${TMPFILE}"

    . "${TMPFILE}" 1>&2 \
    && rm -f "${TMPFILE}"
    unset TMPFILE

}

# ##############################################################################
# Iterator functions

eachmdo () { printMeasure -d "${DOM}" -list | "$@" ; }

# eachsubdo [-l logdir] [-p maxprocs] command [args]
# Remark:
# Pass the domain as {} escaped/quoted inside an argument's value.
eachsubdo () {

    typeset confirm
    typeset logdir="${DS_ENV_LOG}"
    typeset maxprocs=4

    # Options:
    typeset oldind="${OPTIND}"
    OPTIND=1
    while getopts ':l:p:' option ; do
        case "${option}" in
        l) logdir="${OPTARG:-${DS_ENV_LOG}}";;
        p) maxprocs="${OPTARG:-4}";;
        esac
    done
    shift $((OPTIND-1)) ; OPTIND="${oldind}"

    if _any_dir_not_rwx "${DS_ENV_LOG}" ; then
        echo "Log dir '${DS_ENV_LOG}' not rwx (read, write, access)." 1>&2
        return 1
    fi

    dsp -l "${logdir}" -p "${maxprocs}" -z "eachsub-${1}-" "$@" <<EOF
$(domaininfo -d "${DOM}" -shellsubdomains)
EOF
}

# ##############################################################################
# Other functions

# TODO review
#unset fixeofrpas
#fixeofrpas () {
#  fixeofeol -v $(ckeofeol | egrep '(ksh|xml|control)$')
#}
