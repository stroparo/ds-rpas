#!/usr/bin/env bash

# DS-RPAS - Daily Shells Library for RPAS
# License:
#  See README.md document in projects page at
#  https://github.com/stroparo/ds-rpas

# ##############################################################################
# RPAS informational functions

# ##############################################################################
# Domain verification

isdomain () {
    if [ -d "${1:-invalid}/repos" ] ; then
        true
    else
        elog -f -n isdomain "'${1}' is not a domain."
        false
    fi
}

nodomain () {
    if [ ! -d "${1:-invalid}/repos" ] ; then
        elog -n nodomain "'${1}' is not a domain."
        true
    else
        false
    fi
}

# ##############################################################################

# Oneliners:
clonem    () { clonemeas.sh "${1:-invalid}" "${1}" ' \\ \n' ; }
mgrep     () { eachmdo egrep -i "$@" ; }
# printpos1 () { printpos "${1}" | cut -d':' -f1 ; }

allgroups () {

    typeset allgroups="$(mace -d "${1:-${DOM}}" -print -allGroups)"

    # Execute it again to check mace is available, because the typeset command above
    #  shadows error results i.e. $? remains 0:
    mace -d "${DOM}" -print -allGroups >/dev/null 2>&1

    # If mace not available (readonly user) fallback to repos/rules/*.cfg
    if [ "$?" -ne 0 ] ; then
        allgroups=''

        for repo in $(ls -1 ${DOM}/repos/rules/*.cfg) ; do

            group=$(sed -n -e 's/^rulegroup \([^ {]*\) *.*$/\1/p' "${repo}")

            rules="$(sed -n '/^rule [^ ]* {/,/^}/p' ${repo})"
            awk -vgroup="${group:-}" '
                /^rule / { rule = $2; }
                /^"/ {
                    gsub(/(^"|"$)/, "");
                    printf("%-20s|%-20s|%-s\n", group, rule, $0);
                }
            ' "${repo}"
        done
    fi

    awk '
        /^ *Group:/ {
            group = $2;
        }
        /^ *Rule:/ {
            rule = $2;
        }
        /^ *Exp:/ {
            sub(/^ *Exp: */, "");
            printf("%-20s|%-20s|%-s\n", group, rule, $0);
        }
    ' <<EOF
${allgroups}
EOF
}

# Function: arraysizes
# Syntax: {domain} {measure}
# Desc:   Fetch all array sizes of the given measure.
# Deps:   egrep, find, sort
# Output:
#   dom1 -- size
#   dom2 -- size
#   ...
# Retval: standard.
arraysizes () {

    typeset domain="${1}"
    typeset filter='backupdatabases|/users'
    typeset usage='{measure}'

    isdomain "${domain}" || return 10

    ckenv 1 1 "$@"

    echo "Listing arrays in descending size (KiB) order.."

    { echo "${domain}" ; domaininfo -d "${domain}" -shellsubdomains ; } \
    | while read dom
    do
        find "${dom}" -${find_ic}name "${2}"'%1.ary' \
        | while read ary ; do
            if echo "${ary}" | egrep -i -q "${filter}" ; then
                continue
            fi

            echo "$(basename "${dom}") -- $(du -k -s "${ary}")"
        done
    done \
    | sort -n -r -k3

}

# Function _cklog is the internal version for the cklog function.
_cklog () {
    for i in "$@"
    do
        if [ -d "${i}" ]; then

            while read filename; do
                cklog "${filename}"
            done <<EOF
$(find "${i}" -type f)
EOF

        else

            echo "==> ${i}:"

            awk '
                BEGIN { IGNORECASE = 1; }
                /<E|fail|fatal|exited with status|does not exist|conflict|core|] not found|dump|error|except|Except|Unknown option|Permission denied/ \
                && ! /^(<.[^>]*>)?  *Label: |exceptionmask|without error|loglevel error|loglevel '"'"'error'"'"'|RPAS_LOG_LEVEL = error|Position name .* not found.*:/ {
                    print NR ":" $0;
                }
            ' "${i}"

        fi
    done
}

cklog () {
    if [ "$#" -eq 0 ]; then
        _cklog .
    else
        _cklog "$@"
    fi
}

# Function: cleanlog
# Desc:
#  Script that cleans up an rpasInstall log file leaving out the various
#  false success messages, thus making room for a faster debugging.
# Call examples:
#  cleanlog "${BSA_LOG_HOME}"/rpasInstall.RDF.$(date '+%Y%m%d')_1614.log
cleanlog () {

    ckenv 1 1 "$@"

    typeset filter='^<. ......... ..:..:..[.]...>'
    typeset usage='{rpasInstall_log_filename}'

    egrep -v \
    -e '^Test: ' \
    -e '^Checking [^ ][^ ]* [(][^ ][^ ]*[)].' \
    -e '^Copying [^ ][^ ]* to [^ ][^ ]*.' \
    -e '^ *$' \
    -e '${filter} *$' \
    -e "${filter}.*      seconds for " \
    -e '${filter} =========================================================' \
    -e "${filter} Adding function '" \
    -e "${filter} Pos Name" \
    -e "${filter}  pos Label" \
    -e "${filter}   val" \
    -e "${filter}   metric label" \
    -e "${filter} checking to see if measures are registerd....." \
    -e "${filter} Entering Environment::load." \
    -e "${filter} Finished load of '" \
    -e "${filter} input path:" \
    -e "${filter} LoadHier worked" \
    -e "${filter} Looking up metric" \
    -e "${filter} No preload process." \
    -e "${filter} No postload process." \
    -e "${filter} No purge-age for " \
    -e "${filter} Profiling is not enabled" \
    -e "${filter} Starting Env obj." \
    -e "${filter} Starting load of '" \
    -e "${filter} Moving input file '" \
    -e "${filter} No postscrub process." \
    -e "${filter} No prescrub process." \
    -e "${filter} loadmeasure starting to apply loads" \
    -e "${filter} No unapplied loads to apply!" \
    -e "${filter} Registration completed in domain: " \
    -e "${filter} Today: .*Purge-Age:.*Drop:" \
    -e "${filter} Finished load of '" \
    -e "${filter} Loading library '" \
    -e "${filter} Loaded   '" \
    -e "${filter} Environment::load complete" \
    -e "${filter}" \
    -e "${filter}" \
    -e "${filter}" \
    -e '^Adding ' \
    -e '^Appending to ' \
    -e '^ApplicationPrologFrmt$' \
    -e '^Applying load: ' \
    -e '^Calculating Non-materialized measures for RDF.' \
    -e '^Calling CreateGlobalDomain with following arguments:' \
    -e '^Checking permissions on directories for RDF.' \
    -e '^copying input data...' \
    -e '^createGlobalDomain: Done creating partitioned global domain and sub domains' \
    -e '^Creating /' \
    -e '^Creating domain directories for RDF.' \
    -e '^Current domain: ' \
    -e '^Current measure: ' \
    -e '^Done creating domain.' \
    -e '^Executing mace in domain ' \
    -e '^Filtering system hierarchies...' \
    -e '^GenDimLabelOvr.fmt' \
    -e '^GenHierLabelOvr.fmt' \
    -e '^initializing domain arrays...' \
    -e '^Initializing Rpas Jni for domain ' \
    -e '^initializing hierarchies...' \
    -e '^ldrule -d ' \
    -e '^loading hierarchies...' \
    -e '^loading system hierarchies...' \
    -e '^loading translation-related measures...' \
    -e '^Measure [A-Za-z0-9_][A-Za-z0-9_]* was registered.' \
    -e '^Measure [A-Za-z0-9_][A-Za-z0-9_]* was unregistered.' \
    -e '^Now executing' \
    -e '^RDFValidate - End' \
    -e '^RDFValidate - Start' \
    -e '^Registering measures for solution ' \
    -e '^registering RPAS function and expression libraries...' \
    -e '^registering RPAS templates...' \
    -e '^Result: success' \
    -e '^rtkappcnfgmeas -d ' \
    -e '^Registered alert ' \
    -e '^Style config file located at ' \
    -e '^Task Begun: ' \
    -e 'ArrayLoader::run() Loading file' \
    -e '   Conforming = [01].' \
    -e 'Calling Domain::create()...' \
    -e 'completed successfully' \
    -e 'Current file descriptor limit is ' \
    -e 'Entering Level::' \
    -e 'Entering Profile::' \
    -e 'Entering SourceLevel::' \
    -e 'Executing dbbackupmanager in domain ' \
    -e 'Executing loadmeasure in domain ' \
    -e 'File lock timeout set to: ' \
    -e 'Looping over all Dest cells' \
    -e 'MeasureMonitor: output directory is ' \
    -e 'No translation available for ' \
    -e 'Now executing loadHier in domain ' \
    -e 'Now executing regfunction in domain ' \
    -e 'Now executing regtemplate in domain ' \
    -e '^(Adding expression|Ended task|Generating |Processing group)' \
    -e '^(Registering rule group|SHELL Success:|Started task|Task Ended:)' \
    -e '^Writing messages for workbook' \
    -e '^Writing [A-Za-z0-9_][A-Za-z0-9_]* for workbook [A-Za-z0-9_][A-Za-z0-9_]*.' \
    -e "^Attribute '.*' was created." \
    -e "^Position '.*' was created" \
    -e "^Registered user-defined dimension '.*'" \
    -e '^Token Measure .* registered with value' \
    -e '^Validating rule groups for solution [A-Za-z0-9_][A-Za-z0-9_]*.' \
    -e 'Registering [A-Za-z0-9_][A-Za-z0-9_]* measure...' \
    "${1}" > "${1%.log}.clean.log"

}

# Function duwb - Prints the MByte sum used by all workbooks in the DOM master domain.
# Syntax: {domain}
unset duwb
duwb () {
    typeset domain="${1}"

    isdomain "${domain}" || return 10

    find "${domain}" \
        -type d \
        \( -name 't[0-9]' -o -name 't[1-9][0-9]' -o -name 't[1-9][0-9][0-9]' \) \
        -exec du -sm {} \; \
    | getsum . 1
}

findmaces () {

    typeset usage='Just call this with no arguments.'

    ckenv 0 0 "$@"

    {
        echo "${DOM}/bin"
        echo "${DOM}/scripts"
        echo "${PATH}" \
        | tr -s ':' '\n' \
        | grep -v "${DS_HOME}" \
        | grep -v "${RIDE_HOME}" \
        | grep -v "${RPAS_HOME}"
    } \
    | while read dir
    do
        [[ -d ${dir} ]] || continue

        grepctrl="$(egrep -i -n '^[a-z]' "${dir}"/*run*control 2> /dev/null)"
        grepsh="$(egrep -i -n -w 'mace|[-](expression|group|run)' "${dir}"/*sh 2> /dev/null)"

        [ -n "${grepctrl}" -o -n "${grepsh}" ] && echo "==> DIR '${dir}':"

        [[ -n ${grepctrl} ]] && echo "${grepctrl}"

        if [[ -n ${grepsh} ]] && !(echo "${grepsh}" | grep -v ' *#')
        then
            echo "${grepsh}"
        fi

        [ -n "${grepctrl}" -o -n "${grepsh}" ] && echo ''
    done

}

# Function findwblogs
# Syntax: {domain} [yyyymmdd]
findwblogs () {
    typeset domain="${1}"

    isdomain "${domain}" || return 10

    find "${domain}" -type f -name "rpas_D${2:-$(date '+%Y%m%d')}*.log" -ls
}

# Function lsary - lists all arrays in the arg1 domain, ignoring filenames with spaces.
unset lsary
lsary () {
    typeset domain="${1}"

    isdomain "${domain}" || return 10

    lsdomains "${domain}" \
    | while read dom ; do
        find "${dom}/data" -type f \( -name '*[0-9].ary' -o -name 'temp.ary' \)
    done \
    | egrep '/[^ /][^ /]*/(temp|[^ /][^ /]*[0-9][0-9]*)[.]ary'
}

# Function lsarygz - lists all gzipped arrays in the arg1 domain, ignoring filenames with spaces.
unset lsarygz
lsarygz () {
    typeset domain="${1}"

    isdomain "${domain}" || return 10

    lsdomains "${domain}" \
    | while read dom ; do
        find "${dom}/data" -type f \( -name '*[0-9].ary.gz' -o -name 'temp.ary.gz' \)
    done \
    | egrep '/[^ /][^ /]*/(temp|[^ /][^ /]*[0-9][0-9]*)[.]ary.gz'
}

# Function lsdb - lists all databases in the arg1 domain, ignoring filenames with spaces.
unset lsdb
lsdb () {
    typeset domain="${1}"

    isdomain "${domain}" || return 10

    lsdomains "${domain}" \
    | while read dom ; do
        find "${dom}/data" -type d -name '*.db'
    done \
    | grep '/[^ /][^ /]*[.]db'
}

# Function lsdomains - Lists subdomains. Empty arg1 defaults domain to $DOM which is
#  still verified to be a valid domain.
# Options:
#  -a yields all domains with the master displayed in the first line.
#  -r causes raw subdomain paths to be searched ie with no domaininfo/rpas search,
#       but with raw unix commands such as ls and find instead.
#       This option precedes other cases in fact supressing all of them.
# Remark: -p option overrides shellsubdomains, issuing domaininfo -listsubdomains
#  instead, and hence the effects of -a are irrelevant.
# Syntax: [-a|-p] [domain]
lsdomains () {
    typeset domain partitions subs subsfind
    typeset alldomains=false
    typeset pname='lsdomains'
    typeset rawsearch=false

    # Options:
    OPTIND=1
    while getopts ':apr' opt ; do
        case "${opt}" in
        a) alldomains=true;;
        p) partitions=true;;
        r) rawsearch=true;;
        esac
    done
    shift $((OPTIND - 1)) ; OPTIND=1

    domain="${1}"
    isdomain "${domain}" || return 10

    if ${rawsearch} ; then
        subsfind="$(ls -1 -d "${domain}"/*/repos 2> /dev/null)"
        subs="$(echo "${subsfind}" | sed -e 's#/repos$##')"
        ${alldomains:-false} && echo "${domain}"
        echo "${subs}"
    else
        if ${partitions:-false} ; then
            domaininfo -d "${domain}" -listsubdomains || return 80
        else
            ${alldomains:-false} && echo "${domain}"
            domaininfo -d "${domain}" -shellsubdomains || return 80
        fi
    fi
}

# Function lsdomainsxml. Arg1 domain defaults to $DOM when not specified.
# Syntax: [domain]
unset lsdomainsxml
lsdomainsxml () {
    typeset domain="${1:-${DOM}}"

    isdomain "${domain}" || return 10

    awk -F '/|<' '/[0-9][0-9]*.*path>/ { print $(NF-2); }' \
        < "${domain}/config/globaldomainconfig.xml" \
    | sort -n
}

lsgroups () {
    typeset domain="${1:-${DOM}}"

    isdomain "${domain}" || return 10

    usermgr -d "${domain}" -list \
    | awk '
        /^----/ {
            pos++;
        }
        pos == 1 && $0 !~ /^(user|-|$|  *)/ {
            print;
        }
    '
}

# Function lslevels
# Syntax: {domain} [contained-subdomain-basename]
lslevels () {
    typeset domain="${1}"

    isdomain "${domain}" || return 10

    ls -1 "${domain}${2:+/${2}}/data/hmaint.db"/dim_*.ary \
    | sed -e "s/^\(dim\|ext\)_//" -e "s/[.]ary$//"
}

lsusers () {
    typeset domain="${1:-${DOM}}"

    isdomain "${domain}" || return 10

    usermgr -d "${domain}" -list \
    | awk '
        /^----/ {
            pos++;
        }
        pos == 2 && $0 !~ /^(user|-|$|  *)/ {
            print;
        }
    '
}

lsusersaudit () {
    echo 'User,Role(Group),Admin?'

    lsusers \
    | while read u r ; do
        user=$(usermgr -d "${DOM}" -print -user "${u}")

        if echo "${user}" | grep -q 'administrator: *True' ; then
            admin='yes'
        else
            admin='no'
        fi

        group="$(echo "${user}" | awk '/default group:/ { print $NF; }')"

        echo "${u},${group},${admin}"
    done
}

# Function lswb - Prints all workbook directories found in the domain.
unset lswb
lswb () {
    typeset domain="${1}"

    isdomain "${domain}" || return 10

    find "${domain}" \
        -type d \
        \( -name 't[0-9]' -o -name 't[1-9][0-9]' -o -name 't[1-9][0-9][0-9]' \) \
        -ls
}

mgrepbyspecs () {

    ckenv 2 2 "$@"

    typeset usage='{mgrep-regex} {specs-regex}'

    [ "$1" = '-v' ] && verbose=true && shift

    printMeasure -d "${DOM}" -list | egrep -i "${1}" \
    | while read m r ; do
        if specs -t "${m}" ': ' | egrep -i -q "${2}" ; then
            echo "${m} ${r}"

            if [ -n "${verbose}" ] ; then
                specs -t "${m}" ': ' | egrep -i "${2}"
            fi
        fi
    done

    unset verbose

}

minfo () {
    [ -n "$1" ] || return 1

    printMeasureInfo -d "${DOM}" -m "${1}" -setenv "$@"
}

mpop () {
    [ -n "$1" ] || return 1

    typeset dom="${2:-${DOM}}"

    echo "==> $1 pop cells in ${dom##*/}" 1>&2

    mstats "${1}" "${dom}" | \
        tr '\n' ' ' | \
        tr -s ' ' | \
        sed -e "s/printMeasure completed successfully /\\
/g" | \
        sed -e 's/Now executing printMeasure in domain //' | \
        sed -e 's/[.] Number of Dimensions/; #dims/' | \
        sed -e 's/Logical Size/; #cells/' | \
        sed -e 's/Populated Size/; #pop/' | \
        sed -e 's/Percent Populated/; #pop%/' | \
        sed -e 's/  *$//'
}

mpopp () {
    [ -n "$1" ] || return 1

    echo "==> $1 pop cells in all subdomains" 1>&2

    eachsubdo -- -t printMeasure -d '{}' -m "${1}" -baseStats | \
        egrep -v '^INFO:' | \
        tr '\n' ' ' | \
        tr -s ' ' | \
        sed -e "s/printMeasure completed successfully /\\
/g" | \
        sed -e 's/Now executing printMeasure in domain //' | \
        sed -e 's/[.] Number of Dimensions/; #dims/' | \
        sed -e 's/Logical Size/; #cells/' | \
        sed -e 's/Populated Size/; #pop/' | \
        sed -e 's/Percent Populated/; #pop%/' | \
        sed -e 's/  *$//'
}

mpoptest () {
    [ -n "$1" ] || return 1

    typeset mpops
    typeset verbose=false

    # Options:
    typeset oldind="${OPTIND}"
    OPTIND=1
    while getopts ':v' option ; do
        case "${option}" in
        v) verbose=true;;
        esac
    done
    shift $((OPTIND-1)) ; OPTIND="${oldind}"

    for i in "$@" ; do

        mpops="$(mpop $i 2>/dev/null)"

        if echogrep -qv '#pop%: 0 *$' "$mpops" ; then
            echo "$i in master domain"

            if ${verbose} ; then
                echo "$mpops"
                echo ''
            fi
        fi

        mpops="$(mpopp $i 2>/dev/null)"

        if echogrep -qv '#pop%: 0 *$' "$mpops" ; then
            echo "$i in subdomains"

            if ${verbose} ; then
                echo "$mpops"
                echo ''
            fi
        fi
    done
}

mstats () {
    [ -n "$1" ] || return 1
    [ -n "$2" ] || return 2
    pmd "${1}" "${2}" -baseStats | grep -v '^$'
}

# pmd: print measure at domain
pmd () {
    typeset d="${DOM}"
    typeset m="${1}"; shift

    [ -d "${1}/repos" ] && d="${1}" && shift
    [ -d "${d}/repos" ] || return

    printMeasure -d "${d}" -m "${m}" "$@" \
    | egrep -v '^[ \t]*$'
}

# pmsubs: print measure from all subdomains
pmsubs () {
    typeset m="${1}"
    shift

    lsdomains \
    | while read d; do
        pmd "${m}" "${d}" "$@"
    done \
    | egrep -v '^[ \t]*$'
}

pa () {
    case "$1" in
    dim|ext)
        printArray -array "${3:-${DOM}/data/hmaint}.${1}_${2}" -cellsperrow 1
        ;;
    pos)
        pa dim "${2}" \
        | egrep -i '^[(](extname|label)' \
        | sed -e 's/^([^ ][^ ]*  *\(.*\)) *: *"\(.*\)" *$/\1:\2/'
        ;;
    *)
        printArray -array "$@" -cellsperrow 1
        ;;
    esac
}

# Function hgrep
# Purpose:
#   Grep hierarchy dimension (level) arrays for expression, output filename and results.
hgrep () {
    typeset ary res

    while read i ; do
        ary="$(dirname "$i")"
        ary="${ary%.db}.$(basename "${i}" .ary)"

        res="$(printArray -array "$ary" | egrep -i "$1")"

        if [ -n "$res" ] ; then
            echo "==> '$i' <=="
            echo "$res"
        fi
    done <<EOF
$(ls -1 "$DOM"/data/hmaint.db/*.ary)
EOF
}

rg () {

    typeset allg="$(allgroups)"
    typeset green='\033[32m'
    typeset grouprule
    typeset nc='\033[0m'
    typeset pattern="${1}"
    typeset regr

    typeset groupsrules="$(echo "$allg" | egrep -i "${pattern}" | cut -d '|' -f1,2)"

    while read grouprule ; do
        echo

        # TODO echo only when verbose (-v):
        #echo "Group|Rule: '$(echo "${grouprule}" | sed -e 's/  *//g')'"

        regr="$(echo "${grouprule}" | sed -e 's/[|]/[|]/g')"

        echo "$allg" | \
            printawk -F '|' -p "\$0 ~ /^(${regr})/" | \
                awk 'BEGIN { IGNORECASE=1; }
                    {
                        sub(/'"${pattern}"'/, "'"${green}"'&'"${nc}"'"); print;
                    }'
    done <<EOF
${groupsrules}
EOF
    echo
}

rga () { rg "${1}.*([^"'!'"><=]=[^=]|<-)" ; }
rgr () { rg "([^"'!'"><=]=[^=]|<-).*${1}" ; }

# Function:  specs
# Purpose: Display measure specs.
# Arguments examples:
#   actsalsr,clonesalsr '^type' baseint db filename
#   actsalsr :
# TODOs:
# - Implement csv output.
specs () {

    ckenv 2 127 "$@"

    # Params:
    typeset domain="$DOM"
    typeset egrep_regex istabular measures
    typeset usage='{measures_sep_comma} egrep_regex_term+(args sep by space)'

    OPTIND=1
    while getopts ':d:ht' opt ; do
        case "${opt}" in
            d) domain="${OPTARG}" ;;
            h) echo "${usage}" ; return ;;
            t) istabular=true ;;
        esac
    done
    shift $((OPTIND - 1)) ; OPTIND=1

    measures="$(echo "${1}" | tr ',' ' ')"
    shift
    egrep_regex="$(echo "$@" | sed -e 's/ /|/g')"
    egrep_regex="${egrep_regex#|}"
    egrep_regex="${egrep_regex%|}"

    # ######################################################################
    # Main

    if [[ -z ${istabular} ]] ; then

        echo ''

        for i in ${measures} ; do
            echo "${i}"

            printMeasure -d "${domain}" -specs -m "${i}" \
                | egrep "${egrep_regex}" \
                | sort
            echo ''
        done

    else

        for i in ${measures} ; do
            printed="$(printf '%-17s' "${i}")"

            printMeasure -d "${domain}" -specs -m "${i}" \
                | egrep "${egrep_regex}" \
                | sort \
                | sed -e "s/^/${printed}: /"
        done

    fi

}

sps () {
    specs "$@" baseint ^db defagg defspread descr filename label loadint naval ^type
}
