#!/usr/bin/env bash

# DS-RPAS - Daily Shells Library for RPAS
# License:
#  See README.md document in projects page at
#  https://github.com/stroparo/ds-rpas

# ##############################################################################
# DomainDaemon functions

# Oneliners:
ddping    () { daemon -ping ; }
ddps      () { daemon -showActiveServers ; }
ddstop    () { daemon -stop ; }
ddstopps  () { daemon -stopActiveServers ; }

daemon () {
    if [[ ${RPAS_VERSION} = 14* ]] ; then
        DomainDaemon \
            -ipaddr "${IP_ADDRESS}" \
            -port "${RPAS_PORT}" \
            -wallet "${WALLET_DIR}" \
            "$@"
    else
        DomainDaemon \
            -port "${RPAS_PORT}" \
            "$@"
    fi
}

ddstart () {
    if [[ ${RPAS_VERSION} = 14* ]] ; then
        nohup DomainDaemon \
            -ipaddr "${IP_ADDRESS}" \
            -port "${RPAS_PORT}" \
            -ssl 2 \
            -start \
            -wallet "${WALLET_DIR}" \
            > "${RPAS_LOGS:-${HOME}}/daemon_$(date "+%Y%m%d_%OH%OM").log" \
            2>&1 &
    else
        nohup DomainDaemon \
            -port "${RPAS_PORT}" \
            -start \
            > "${RPAS_LOGS:-${HOME}}/daemon_$(date "+%Y%m%d_%OH%OM").log" \
            2>&1 &
    fi
}
