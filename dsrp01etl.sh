#!/usr/bin/env bash

# DS-RPAS - Daily Shells Library for RPAS
# License:
#  See README.md document in projects page at
#  https://github.com/stroparo/ds-rpas

# ##############################################################################
# ETL functions

exph () {
    typeset filename
    typeset hier
    typeset timestamp="$(date '+%y%m%d%OH%OM')"

    for hier in "$@" ; do
        filename="${hier}.csv.dat.${timestamp}"
        echo "==> ${filename}" 1>&2
        exportHier -d "${DOM}" -hier "${hier}" -datFile "${filename}"
    done
}

exphgz () {
    typeset filename

    exph "$@" 2>&1 | grep '^==>' | awk '{print $NF;}' > "$HOME/.exphgz"

    while read filename ; do
        gzip -c "${filename}" > "${filename%.*}.gz" && rm -f "${filename}"
    done < "$HOME/.exphgz"

    wait && rm -f "$HOME/.exphgz" 2>/dev/null
}

# expm {measure} [other-exportMeasure arguments..]
expm () {

    typeset m="${1}"
    shift 1

    typeset intx="$(printMeasureInfo -d "${DOM}" -measure "${m}" -baseint)" || return $?

    exportMeasure -d "${DOM}" -meas "${m}" -out "${m}" -intx "${intx}" "$@"
}

expmati () {
    m="${1}"
    int="${2}"
    shift 2

    exportMeasure -d "${DOM}" -meas "${m}" -out "${m}" -intx "${int}" "$@"
}

# rmfixedwidth function makes CSV take precedence over fixed format files.
rmfixedwidth () {

    find "${DOM}/input" -type f \
    | grep -i -v 'csv' \
    | egrep '[.](clr|inc|ovr|rpl)$' \
    | while read filename
    do
        rm -f "${filename}" 2>/dev/null \
        && echo "Removed '${filename}'."
    done

}

rminputsubs () {

    domaininfo -d "${DOM}" -shellsubdomains \
    | while read subdomain ; do
        cd "${subdomain}/input"
        rm -f *.clr *.inc *.ovr *.rpl
    done

}
