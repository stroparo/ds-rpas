#!/usr/bin/env bash

# DS-RPAS - Daily Shells Library for RPAS
# License:
#  See README.md document in projects page at
#  https://github.com/stroparo/ds-rpas

# ##############################################################################
# Params AFTER INIT - safeguards and/or depend on an environment that is already initiated:

export DOMPARENT="$(dirname "${DOM}")"

if which rpasversion >/dev/null 2>&1 ; then
    export RPAS_VERSION="$(rpasversion -version|grep 'Version:'|awk '{print $NF}')"
fi

if [ -z "${IP_ADDRESS}" ] ; then
    export IP_ADDRESS="$(ifconfig -a|grep -w "inet"|grep -v 127.0.0.1|head -1|cut -d" " -f2)"
fi

if [[ $(ifconfig -a|grep -w "inet"|grep -v 127.0.0.1|wc -l) -gt 1 ]]; then
    echo "More than one inet addr found, using first (${IP_ADDRESS})." 1>&2
fi

[ -z "${RPAS_TODAY}" ] && export RPAS_TODAY="$(date '+%Y%m%d')"
[ -z "${WALLET_DIR}" ] && export WALLET_DIR="${HOME}/.wallets/server"

[ -z "${session_timestamp}" ] && export session_timestamp="$(date '+%Y%m%d%OH%OM%OS')"

# PS1 shell prompt:
if [ -n "$BASH_VERSION" ] ; then

    new_ps1='[\[\e[32m\]\u@\h\[\e[0m\] \t \$?=$? \W]\$ '

    case "${which_env}" in
        con|prd) new_ps1='[\[\e[31m\]\u@\h\[\e[0m\] \t \$?=$? \W]\$ ' ;;
        uat) new_ps1='[\[\e[33m\]\u@\h\[\e[0m\] \t \$?=$? \W]\$ ' ;;
    esac
else
    new_ps1='$USER@'"$(hostname)"' \$?=$? $PWD\$ '
fi

export PS1="${new_ps1}"

# ##############################################################################
# Aliases

# alias bin:
[ -d "${DS_ENV}/bin" ] && alias bin='d "${DS_ENV}/bin" -A'
[ -d "${DOM}/script" ] && alias bin='d "${DOM}/script" -A'
[ -d "${DOM}/scripts" ] && alias bin='d "${DOM}/scripts" -A'
[ -d "${DS_ENV}/script" ] && alias bin='d "${DS_ENV}/script" -A'
[ -d "${DS_ENV}/scripts" ] && alias bin='d "${DS_ENV}/scripts" -A'

# ##############################################################################
# Post calls

appendto 'DS_POST_CALLS' '_rpas_aliases'

# ##############################################################################

set -b
set -o vi

if ! cd "${DOM}" ; then
    echo 'Falling back to parent..' 1>&2
    cd "${DOMPARENT:-${DOM%/*}}"
fi
