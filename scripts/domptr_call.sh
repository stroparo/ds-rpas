#!/usr/bin/env bash

# DS-RPAS - Daily Shells, RPAS Variant
# License: GNU GPL v2
# Author: CS

# File with sample calls to the domptr script.

domptr \
   -d /path \
   -l 'ldom01' \
   -m 'MASTERDOM' \
   -p 'SCLS' \
   -r '/AIP'

domptr \
  -b /from_other_folks_raw \
  -d /path \
  -f 'xz' \
  -l 'ldom01' \
  -m 'MASTERDOM' \
  -p 'SCLS' \
  -r '/ldom'
