#!/usr/bin/env bash

# DS-RPAS - Daily Shells Library for RPAS
# License:
#  See README.md document in projects page at
#  https://github.com/stroparo/ds-rpas

# ##############################################################################

# Purpose:
# Repackage domain after cleaning up interface files and purging workbooks.

# ##############################################################################

export PROGRAM=$(basename "$0")
export USAGE="Function domrepack - Cleans and archives the domain into SOURCE-tgz-YYMMDD-HHMM

Syntax:
${PROGRAM} [-c] [-p {maxprocs}] [-r] {domain}

Options:
    -c      # clean the domain up before packing.
    -p {maxprocs[=16]}
    -r      # triggers removal of the source domain after packing.
"

# ##############################################################################
# Functions

prep () {

    typeset oldpwd="$PWD"

    if ! . "${DS_HOME}/ds.sh" "${DS_HOME}" >/dev/null 2>&1 || [ -z "${DS_LOADED}" ] ; then
        echo "${PROGRAM}: FATAL: Could not load DS - Daily Shells." 1>&2
        exit 1
    fi

    cd "$oldpwd"
}

unset domrepack
domrepack () {
    typeset pname=domrepack

    typeset doclean=false
    typeset doremove srcdir destdir
    typeset maxprocs=16

    # Options:
    OPTIND=1
    while getopts ':chp:r' opt ; do
        case "${opt}" in
            c)
                doclean=true;;
            h)
                echo "$USAGE" 1>&2
                exit 0
                ;;
            p)
                maxprocs="${OPTARG}";;
            r)
                doremove=true;;
        esac
    done
    shift $((OPTIND - 1)) ; OPTIND=1

    srcdir="$(cd "${1}" ; echo $PWD)"
    destdir="${srcdir}-tgz-$(date '+%y%m%d-%OH%OM')"

    if ${doclean} ; then
        domclean -p ${maxprocs} "${srcdir}" || return 10
    fi

    elog -n "${pname}" "Archiving domain to '${destdir}'.."
    childrentgz -p ${maxprocs} "${srcdir}" "${destdir}"

    wait

    if catnum "${destdir}"/*log | grep -qv 0 ; then
        elog -f -n "$pname" "There were errors, check the '${destdir}/*log' logs."
        return 1
    elif [ "${doremove}" = true ] ; then
        rm -rf "${srcdir}"
    fi
}

# ##############################################################################
# Main

prep
domrepack "$@"
exit "$?"
