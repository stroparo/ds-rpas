#!/usr/bin/env bash

# dryconfig [dir1 [dir2 ...]]
dryconfig () {
    for dir in "$@"
    do
        find "${dir}" -name '*easures.xml' -o -name 'classScheme.xml' | \
        while read file
        do
            python "${DS_RPAS}"/filter_measures_xml.py "${file}" > "${file}".filt
            mv "${file}".filt "${file}"
        done
    done
}
dryconfig "$@"
