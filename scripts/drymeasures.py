import re
import sys

filename = sys.argv[1]

s = ''.join(open(filename).readlines())

ignore_list = (\
    r'\s*<attribute name="materialized">\s*<value>(?:DISPLAY|PERSISTENT)</value>\s*</attribute> *',
    r'\s*<attribute name="(?:previous[^"]*|periodstartvalue)">\s*<value></value>\s*</attribute> *',
)

for i in ignore_list:
    s = re.sub(i, '', s, flags=(re.I))

print s,
