#!/usr/bin/env bash

# Authors: CM, CS, LV

# Log:
# 2013-08-22: CS Added behaviour for attribute "Measure Attributes" (== attr)

usage="DOMAIN MEASURE [SEPARATOR]"

if [ "$#" -eq 2 ]
then
	PATH_TO_DOMAIN="$1"
	measure="$2"
	sep=" "
elif [ "$#" -eq 3 ]
then
	PATH_TO_DOMAIN="$1"
	measure="$2"
	sep="$3"
else
	echo "${usage}"
	exit
fi

printMeasure -d "${PATH_TO_DOMAIN}" -m "${measure}" -specs | grep ":" | grep -v "SPECS FOR MEASURE:" | awk '
BEGIN{
	sq="'\''";
	sq2="'\'\''";
	dq="\"";
	FS=":";
	tokenmeas=0;
	result="";
	sep="'"$sep"'";
}
{
	attr=$1;
	sub(/^[a-z_][a-z_]*: */,"");
	sub(/^Measure Attributes: */,"");
	if(attr == "label"){
		result=result sep "-label " sq $0 sq;
	}else if(attr == "type"){
		split($0,tmp,"[)(]");
		result=result sep "-type " tmp[2];
	}else if(attr == "baseint"){
		result=result sep (($0 != sq2)?"-baseint " $0:"-scalar");
	}else if(attr == "navalue"){
		result=result sep "-navalue " sq dq $0 dq sq;
	}else if(attr == "defagg"){
		result=result sep "-defagg " $0;
	}else if(attr == "defspread"){
		result=result sep "-defspread " $0;
	}else if(attr == "allowedaggs"){
		result=result sep "-allowedaggs " $0;
	}else if(attr == "range"){
		result=result (($0 == 43)?"":sep (($0 == sq2)?"-range " sq2:"-range " sq "[" $0 "]" sq ));
	}else if(attr == "refreshable"){
		result=result sep "-refreshable " $0;
	}else if(attr == "db"){
		result=result sep "-db " sq $0 sq;
	}else if(attr == "insertable"){
		result=result sep "-insertable " $0;
	}else if(attr == "basestate"){
		result=result sep "-basestate " (($0 == 2)?"write":"read");
	}else if (attr == "aggstate"){
		result=result sep "-aggstate " (($0 == 2)?"write":"read");
	}else if(attr == "stageonly"){
		result=result sep "-stageonly " $0;
	}else if(attr == "filename"){
		result=result sep "-filename " sq $0 sq;
	}else if(attr == "loadint"){
		result=result sep "-loadint " $0;
	}else if(attr == "clearint"){
		result=result sep "-clearint " $0;
	}else if(attr == "loadstokeep"){
		result=result sep "-loadstokeep " $0;
	}else if(attr == "start"){
		result=result sep "-start " $0;
	}else if(attr == "width"){
		result=result sep "-width " $0;
	}else if(attr == "loadagg"){
		result=result sep "-loadagg " $0;
	}else if(attr == "purgeage"){
		result=result (($0 <  0)?"":sep "-purgeage " sq $0 sq );
	}else if(attr == "viewtype"){
		result=result sep "-viewtype " $0;
	}else if(attr == "syncwith"){
		result=result sep "-syncwith " $0;
	}else if(attr == "description"){
		result=result sep "-description " sq $0 sq;
	}else if(attr == "ui_type"){
		result=result (($0 == "1 (picklist)")?sep "-picklist":"");
	}else if(attr == "materialized"){
		tmp_arr[0]="persistent";
		tmp_arr[1]="display";
		tmp_arr[2]="readonly";
		tmp_arr[3]="readwrite";
		result_NOT_USED=result sep "-materialized " tmp_arr[$0];
	}else if(attr == "lowerbound"){
		result=result sep "-lowerbound " $0;
	}else if(attr == "upperbound"){
		result=result sep "-upperbound " $0;
	}else if(attr == "tokenmeas"){
		if ($0 == "true") {
			tokenmeas = 1;
		}
	}else if(attr == "fnhbi"){
		result=result (($0 == "true")?sep "-fnhbi":"");
	}else if(attr == "scriptname"){
		result=result sep "-scriptname " $0;
	}else if(attr == "hybridaggspec"){
		result=result sep "-hybridaggspec " $0;
	}else if(attr == "periodstartvalue"){
		result=result sep "-periodstartvalue " $0;
	}else if(attr == "Measure Attributes"){
		split($0, attrs, " ");

		for (i in attrs) {
			split(attrs[i], attrkeyval, "(");

			attrpos = attrkeyval[2];
			sub(/[)]$/, "", attrpos);

			result=result sep "-attr " attrkeyval[1] " -attrpos " attrpos;
		}
	}else{
		print "*** ATTRIBUTE NOT RECOGNIZED AND IGNORED: " attr;
	}
}
END{
	if ( tokenmeas == 1)
		print "*** This script could not recreate token type measures. TVN";
	else
		print "regmeasure -d '\'${PATH_TO_DOMAIN}\'' -add '\'${measure}\''" result;
}'
