#!/usr/bin/env bash

# DS-RPAS - Daily Shells, RPAS Variant

usage='Display help via -h option.'
. "${DS_HOME}/ds.sh"

ckenv 0 127 "$@"

usage () {
  cat <<'EOF'
cpstyles.sh script

DESCRIPTION:

Script that copies styles from a source domain (-d) to a destination master
domain specified via option -t {target-domain} (default is to refer to the
master domain pointed to by the DOM environment variable).

The script waits on RPAS sessions (RpasDbServer processes) to finish before
starting the copy procedure.

OPTIONS:
[-a {all|apply|backup|backup-apply|make}] ; Default action: make
[-d {sourceDom|domPathSuffix}] ; Default source domain: "${DOM}"
[-s {scope=groups|template|users}] ; Default scope: template
[-t {targetDom|domPathSuffix}] ; Default target master domain: "${DOM}"
[-w {template name}] ; Optional, and will filter to only include the workbook with such specified RPAS' internal name.

EXAMPLES:
cpstyles.sh -d someldom -a all
cpstyles.sh -d someldom -a apply
cpstyles.sh -d someldom
cpstyles.sh -d AIP0000
cpstyles.sh -d MFP0003

REMARKS:
Although this has been tested on IBM AIX, this scripts gives warranties only
against GNU's implementation of the 'tar' + 'gzip' commands.
EOF
}

# ######################################################################
# Params:

export CPSTYLES_HOME="${HOME}/.cpstyles"

while [ "$#" -gt 0 ] ; do
  case "${1}" in
    -a) action="${2}" ; shift 2 ;;
    -d)
      if [ -d "${2}/repos" ] ; then
        source_domain="${2}"
      elif [ -d "${DOM}${2}/repos" ] ; then
        source_domain="${DOM}${2}"
      else
        echo "Domain value invalid: '${2}'. Aborted."
        exit 1
      fi
      shift 2
      ;;
    -h) usage ; exit 0 ;;
    -r) reap_thru=true ; echo $reap_thru ; shift ;;
    -s) scope="${2}" ; shift 2 ;;
    -t) target_master_domain="${2}" ; shift 2 ;;
    -w) wbname="${2}" ; shift 2 ;;
    *) shift ;;
  esac
done
: ${action:=make}
: ${scope:=template}
: ${source_domain:=${DOM}}
: ${target_master_domain:=${DOM}}
source_domain_styles_path="${source_domain}/styles"
backup_ident="_$(date '+%Y%m%d_%OH%OM%OS')"
#echo "${source_domain_styles_path}"

# ######################################################################
# Checks (preemptive):
if [ ! -d "${target_master_domain}" ] ; then
  echo "Directory '${target_master_domain}' is missing!"
  exit 1
fi
if [ ! -d "${source_domain_styles_path}" ] ; then
  echo "Directory '${source_domain_styles_path}' is missing!"
  exit 1
fi

# Wait for RPAS processes (sessions) to finish:
if [ -z "${reap_thru}" ] ; then
  echo "Waiting for RPAS sessions at $(date +'%OH:%OM on %Y-%m-%d..') ..."
  while ps -ef | grep -q RpasDbServer ; do
    sleep 8
  done
  echo 'No RPAS sessions are active, so proceeding..'
fi

# Checks (reactive):
[ -d "${CPSTYLES_HOME}" ] || mkdir "${CPSTYLES_HOME}" || exit 2

# ######################################################################
# Main program:

cpstyles () {

  case "${action}" in

    all)
      assembleFromSource
      backupDestination
      applyToDestination
      ;;

    apply)
      applyToDestination
      ;;

    backup)
      backupDestination
      ;;

    backup-apply)
      backupDestination
      applyToDestination
      ;;

    make)
      assembleFromSource
      ;;

    *)
      echo 'No action selected. Nothing has been done.'
      ;;

  esac

}

# ######################################################################
# Auxiliary functions:

assembleFromSource () {

  find "${source_domain_styles_path}" -type d -name "${scope}" | \
  while read d ; do
    cd "${d}/.."
    styles_wb_basename="${PWD##*/}"
    if echo "${styles_wb_basename}" | grep -i -q "${wbname}" ; then
      tar -cf - "${scope}" | gzip -c > "${source_domain_styles_path}/${scope}_${styles_wb_basename}.tar.gz"
    fi
  done

  # Copy to temporary directory:
  rm -f "${CPSTYLES_HOME}"/"${scope}"_*.tar.gz
  cp -f "${source_domain_styles_path}"/"${scope}"_*.tar.gz* "${CPSTYLES_HOME}"/ || exit 90
  echo "Assembled '${scope}' style archives from domain '${source_domain}':"
  ls -1d "${CPSTYLES_HOME}"/"${scope}"_*.tar.gz

}

backupDestination () {

  for d in "${target_master_domain}" $(domaininfo -d "${target_master_domain}" -shellsubdomains) ; do
    cd "${d}"
    if tar -cf - styles | gzip -c > "styles${backup_ident}.tar.gz" ; then
      echo "Backed up '$(ls -1 "${d}/styles${backup_ident}.tar.gz")'"
    else
      echo "Backup failed for '${d}/styles'"
    fi
  done
  #find "${target_master_domain}" -type f -name 'styles*.tar.gz' | sort

}

applyToDestination () {

  # First delete previous contents in the destination:
  for d in "${target_master_domain}" $(domaininfo -d "${target_master_domain}" -shellsubdomains) ; do
    ls -1 "${CPSTYLES_HOME}"/"${scope}"_*.tar.gz | grep -i -q "${wbname}" | \
      while read styles_archive ; do
        wb="${styles_archive##*/}"
        wb="${wb#*_}"
        wb="${wb%.tar.gz}"
        echo "Removing '${d}/styles/${wb}/${scope}', press ENTER to continue.."
        read dummy
        rm -f -r "${d}/styles/${wb}/${scope}" && echo "Removed '${d}/styles/${wb}/${scope}'"
      done
  done

  # Then uncompress to destinations:
  ls -1 "${CPSTYLES_HOME}"/"${scope}"_*.tar.gz | \
    while read tarfile ; do
      styledir="${tarfile##*/${scope}_}"
      styledir="${styledir%%.tar.gz}"
      for domdir in "${target_master_domain}" $(domaininfo -d "${target_master_domain}" -shellsubdomains) ; do
        #ls -1d "${domdir}/styles/${styledir}" ; continue
        cd "${domdir}/styles/${styledir}" && \
          (gunzip -c "${tarfile}" | tar -xf -) && \
            echo "Deployed '$(ls -1d "${PWD}/${scope}")'"
      done
    done

}

# ######################################################################

cpstyles "$@"
