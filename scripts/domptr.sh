#!/usr/bin/env ksh

# DS-RPAS - Daily Shells, RPAS Variant
# License: GNU GPL v2
# Author: CS

# Script: Domain Repointer
# Desc:   Repoint domains to execute maintenance and support.

# Made this a subdomain iterator:
#export LOCAL_DOMAIN='/u01/transf/SOMEDOMAIN'

# Params adjusted:
export BACKUP_DIR=''
export BACKUP_FILE_TYPE=''
export LOCAL_DOMAIN_LIST=''
export MASTER_DOMAIN=''
export PDIM="SCLS"

# RE_LSD stands for RegEx Local SubDomains (to filter domaininfo's output):
export RE_LSD='/ldom'

export WKDIR="${HOME}/.refresh"

while getopts ':b:d:f:hl:m:p:r:sw:' opt ; do
  case "${opt}" in
    b) export BACKUP_DIR="${OPTARG}" ;;
    d) export DEST_DIR="${OPTARG}" ;;
    f) export BACKUP_FILE_TYPE="${OPTARG}" ;;
    h)
cat <<EOF

  domain_repointer.sh - a program that repoints domains moved without copyDomain.

  This is a script to facilitate the process, it is not meant to guess
  totally random (stupid) domains' basenames setup by a domain owner.

  Options:
    -b {backup directory}
      Dir that contains backup domain archives (.tar.gz|.tar.xz) or dirs.
      If not specified use the (-d) dir as the backup dir: in this case,
      if -f is not specified the regular copy will not be done i.e.
      as the directories are already in place (BE CAREFUL and certain
      that you have your backups placed somewhere else).

    -d {deploy directory}

    -f {gz|xz}
      If not specified but -b present (!= -d) then do a regular (recursive) copy.
      Backup file type:
        gz=gzip
        xz=xzip

    -l {local domain basenames list}
      Local domain base-basenames (ex. "DOM001 DOM002" etc.).
      Separated by either commas, newlines (0x0A) or regular spaces.

    -m {master domain basename} (ex. "DOM000")

    -p {partition dimension}
      This must be in uppercase characters.
      Example: -p SCLS

    -r {listsubdomains_regex} (ex. "/DOM")

EOF
      exit ${EXIT_STATUS_SUC:-0}
      ;;
    l) export LOCAL_DOMAIN_LIST=$(echo "${OPTARG}" | tr -s ',' ' ') ;;
    m) export MASTER_DOMAIN="${OPTARG}" ;;
    p) export PDIM="${OPTARG}" ;;
    r) export RE_LSD="${OPTARG}" ;;
    s) export SUBDIRS_TO_MASTER=true ;;
    w) export WKDIR="${OPTARG}" ;;
  esac
done
shift $((OPTIND-1)) ; OPTIND=1

# Params fixed or from system etc.:
export EXIT_STATUS_ERR=1
export EXIT_STATUS_SUC=0
export TSTAMP="$(date '+%Y%m%d%OH%OM')"

# Params derived:
export F_INDX_LDOM="${WKDIR}/indx_ldom_hash.csv"
export F_LOG="${WKDIR}/repoint_${TSTAMP}.log"
export F_LOG_RECONFIG="${F_LOG%.log}_reconfig.log"
export F_LSD="${WKDIR}/listsubdomains"
export F_POS_PARTDIM="${WKDIR}/pos_partdim"
export F_POS_REPOINT="${WKDIR}/pos_repoint"
export F_POS_REPOINT_LDOM="${F_POS_REPOINT}_ldom"
export F_POS_RM="${WKDIR}/pos_rm"
export SCRIPT_RESET_INDICES="${WKDIR}/reset_indices.sh"
export SCRIPT_RESET_PATHS="${WKDIR}/reset_paths.sh"
export SCRIPT_SET_INDICES="${WKDIR}/set_indices.sh"
export SCRIPT_SET_PATHS="${WKDIR}/set_paths.sh"

# Params derived - specific:
export ARR_DOM_INDX="${DEST_DIR}/${MASTER_DOMAIN}/data/configmeasdata.r_subdomainindex"
export ARR_DOM_PATH="${DEST_DIR}/${MASTER_DOMAIN}/data/configmeasdata.r_subdomainpath%1"
# TODO derive it from PDIM param.:
export ARR_PDIM_EXT="${DEST_DIR}/${MASTER_DOMAIN}/data/hmaint.ext_scls"

# Support Functions:
exec_perm_to () {
  chmod +x "${1}"
  verify_exec "$?" "Exec perm assignment failure on '${1}'."
}

exec_script () {
  exec_perm_to "${1}"
  "$@"
  verify_exec "$?" "Failed exec of '${1}'. See '${1}.log'."
}

# Ignores errors
exec_script_tolerant () {
  exec_perm_to "${1}"
  "$@"
}

exit_error () {
  exit ${EXIT_STATUS_ERR:-1}
}

log_echo () {

  while getopts ':q' opt ; do
    case "${opt}" in
      q) log_echo_ignore=true ;;
    esac
  done
  shift $((OPTIND-1)) ; OPTIND=1

  if [ -f "$1" ] ; then
    log_echo_file="$1" ; shift
  else
    log_echo_file="${F_LOG}"
  fi

  if [ -n "${log_echo_ignore}" ] ; then
    echo "$@" >> "${log_echo_file}"
  else
    echo "$@" | tee -a "${log_echo_file}"
  fi

  unset log_echo_file log_echo_ignore

}

init_log () {
  > "${F_LOG}"
}

init_workspace () {
  [ -d "${WKDIR}" ] || mkdir -p "${WKDIR}"
}

# Usage: {file1} {file2}
# Results: exits the script with a log message if the line counts differ.
verify_equal_cardinality () {

  [ -f "${1}" ]
  verify_exec "$?" "'${1}' missing!"
  [ -f "${2}" ]
  verify_exec "$?" "'${2}' missing!"

  count_err="$(wc -l "${1}" | cut -d' ' -f1)"
  count_ext="$(wc -l "${2}" | cut -d' ' -f1)"
  [ ${count_err} -eq ${count_ext} ]

  verify_exec "$?" "count($(basename "${1}")) differs from count($(basename "${2}"))!"
  unset count_err count_ext

}

# Usage: "$?" {program_name|error_message}
# Results: log and exit on error.
verify_exec () {
  if [ "$1" -ne 0 ] ; then
    log_echo "FATAL: '${2}'; exit status = '${1}'"
    exit_error
  fi
}

# Main Functions:

repoint_prepare_env () {

  which domaininfo > /dev/null 2>&1
  verify_exec "$?" 'domaininfo executable is missing!'

  export PATH="${HOME}/opt/bin:${PATH}"
  export PATH="$(echo "${PATH}" | tr -s ':' '\n' | grep -F 'rpas/bin')/.private:${PATH}"

  which populateArray > /dev/null 2>&1
  verify_exec "$?" 'populateArray executable is missing!'

}

# Grep -listsubdomains results (this is a domaininfo option):
lsd_grep () {
  egrep "$@" | cut -d' ' -f2- | tr -s ' ' '\n' | grep -v '^$'
}

# Usage: {domain regexes file} {listsubdomains file}
set_pos_ldom_hash () {

  egrep -f "${1}" "${2}" > "${2}_targeted"

  while read ldom_path positions ; do
    echo "${positions}" | tr -s ' ' '\n' | grep -v '^$' | \
    awk -v ldom="${ldom_path##*/}" '{ print $0 ":" ldom ; }'
  done < "${2}_targeted"

  rm -f "${2}_targeted"

}

helper_enforcer () {

  # Cheapest tests go here at the top:

  echo "${BACKUP_FILE_TYPE}" | egrep -q '^(gz|xz)$|^$'
  verify_exec "$?" 'grep: invalid backup file type!'

  [ -n "${PDIM}" ]
  verify_exec "$?" 'Partition dimension cannot be null!'

  # Most expensive operations go below this point:

  export BACKUP_DIR="${BACKUP_DIR:-${DEST_DIR}}"
  [ -d "${BACKUP_DIR}" -a -r "${BACKUP_DIR}" -a -x "${BACKUP_DIR}" ]
  verify_exec "$?" "BACKUP_DIR='${BACKUP_DIR}' not accessible!"

  BDU="${DEST_DIR}"
  [ -d "${BDU}" -a -r "${BDU}" -a -w "${BDU}" -a -x "${BDU}" ]
  verify_exec "$?" "DEST_DIR='${DEST_DIR}' not accessible!"

  # Source domains exist and are accessible?
  if [ -n "${BACKUP_FILE_TYPE}" ] ; then
    for i in ${MASTER_DOMAIN} ${LOCAL_DOMAIN_LIST} ; do
      backup_file="$(ls -1 "${BACKUP_DIR}/${i}.tar.${BACKUP_FILE_TYPE}"* | head -1)"
      [ -f "${backup_file}" -a -r "${backup_file}" ]
      verify_exec "$?" "Backup files '${BACKUP_DIR}/${i}.tar.${BACKUP_FILE_TYPE}'* not accessible!"
    done
    unset backup_file
  else
    for i in ${MASTER_DOMAIN} ${LOCAL_DOMAIN_LIST} ; do
      [ -d "${BACKUP_DIR}/${i}" ]
      verify_exec "$?" "Source domain path '${BACKUP_DIR}/${i}' is not a directory!"
      [ -r "${BACKUP_DIR}/${i}" -a -w "${BACKUP_DIR}/${i}" -a -x "${BACKUP_DIR}/${i}" ]
      verify_exec "$?" "Source domain directory '${BACKUP_DIR}/${i}' not accessible!"
    done
  fi

}

helper_source_provider () {

  DEST_DIR_LOCALS="${DEST_DIR}${SUBDIRS_TO_MASTER:+/${MASTER_DOMAIN}}"

  log_echo "Restoring backups (if any) in '$(basename "${BACKUP_DIR}")' ..."

  if [ "${BACKUP_FILE_TYPE}" = 'gz' ] ; then
    cd "${DEST_DIR}"
    cat "${BACKUP_DIR}/${MASTER_DOMAIN}.tar.${BACKUP_FILE_TYPE}"* | gunzip -c | tar -xf -
    verify_exec "$?" "Failed uncompressing '${BACKUP_DIR}/${MASTER_DOMAIN}.tar.${BACKUP_FILE_TYPE}'*"

    cd "${DEST_DIR_LOCALS}"
    for i in ${LOCAL_DOMAIN_LIST} ; do
      cat "${BACKUP_DIR}/${i}.tar.${BACKUP_FILE_TYPE}"* | gunzip -c | tar -xf -
      verify_exec "$?" "Failed uncompressing '${BACKUP_DIR}/${i}.tar.${BACKUP_FILE_TYPE}'*"
    done
  elif [ "${BACKUP_FILE_TYPE}" = 'xz' ] ; then
    cd "${DEST_DIR}"
    cat "${BACKUP_DIR}/${MASTER_DOMAIN}.tar.${BACKUP_FILE_TYPE}"* | xz -c -d | tar -xf -
    verify_exec "$?" "Failed uncompressing '${BACKUP_DIR}/${MASTER_DOMAIN}.tar.${BACKUP_FILE_TYPE}'*"

    cd "${DEST_DIR_LOCALS}"
    for i in ${LOCAL_DOMAIN_LIST} ; do
      cat "${BACKUP_DIR}/${i}.tar.${BACKUP_FILE_TYPE}"* | xz -c -d | tar -xf -
      verify_exec "$?" "Failed uncompressing '${BACKUP_DIR}/${i}.tar.${BACKUP_FILE_TYPE}'*"
    done
  else
    if [ "${BACKUP_DIR}" != "${DEST_DIR}" ] ; then
      cd "${DEST_DIR}"
      cp -p -R "${BACKUP_DIR}/${MASTER_DOMAIN}" ./
      verify_exec "$?" "Failed copying '${BACKUP_DIR}/${MASTER_DOMAIN}'."

      cd "${DEST_DIR_LOCALS}"
      for i in ${LOCAL_DOMAIN_LIST} ; do
        cp -p -R "${BACKUP_DIR}/${i}" ./
        verify_exec "$?" "Failed copying '${BACKUP_DIR}/${i}'."
      done
    fi
  fi

  [ -d "${DEST_DIR}/${MASTER_DOMAIN}" ]
  verify_exec "$?" "Supposedly new master domain is missing!"

}

# Based moostly on domaininfo -d {dom} -listsubdomains resulting paths:
# '/{DOMAIN_DIR_BASENAME}':
repoint_prepare () {

  helper_enforcer
  helper_source_provider

  # F_LSD - listsubdomains file assembly:
  domaininfo -d "${DEST_DIR}/${MASTER_DOMAIN}" -listsubdomains | grep "${RE_LSD}" | \
    egrep -i -v 'domaininfo|is partitioned on' > "${F_LSD}"

  # Regexes to filter domains in F_LSD, to generate position lists
  # and then generate a (partdim_pos:ldom) hash:
  > "${F_LSD}_re_locals"
  for i in ${LOCAL_DOMAIN_LIST} ; do
    echo "/${i}" >> "${F_LSD}_re_locals"
  done
  lsd_grep    -f "${F_LSD}_re_locals" "${F_LSD}" > "${F_POS_REPOINT}"
  lsd_grep -v -f "${F_LSD}_re_locals" "${F_LSD}" > "${F_POS_RM}"
  cat "${F_POS_REPOINT}" "${F_POS_RM}" > "${F_POS_PARTDIM}"
  set_pos_ldom_hash "${F_LSD}_re_locals" "${F_LSD}" > "${F_POS_REPOINT_LDOM}"

}

# Mark path removals (via 'point-to-master, remove-only-wanted' technique):
repoint_reset_paths () {

  log_echo "MASTER_PATH='${DEST_DIR}/${MASTER_DOMAIN}'"

  log_echo "'${SCRIPT_RESET_PATHS}' build and exec ..."
  echo "rm -f '${SCRIPT_RESET_PATHS}.log' 2>/dev/null" > "${SCRIPT_RESET_PATHS}"

  rm -f "${F_POS_PARTDIM}".err 2> /dev/null

  while read pos ; do
cat >> "${SCRIPT_RESET_PATHS}" <<EOF
populateArray -array '${ARR_DOM_PATH}' -set '${PDIM}:${PDIM}${pos}' -type string -value '${DEST_DIR}/${MASTER_DOMAIN}' >> '${SCRIPT_RESET_PATHS}.log' 2>&1
[ "\$?" -ne 0 ] && echo '${pos}' >> '${F_POS_PARTDIM}'.err
EOF
  done < "${F_POS_PARTDIM}"

  exec_script_tolerant "${SCRIPT_RESET_PATHS}"

  # Ext cells remaining as ext_PDIM cells external names throw
  # an error when they are in 'populateArray -set ...':
  printArray -array "${ARR_PDIM_EXT}" | grep -f "${F_POS_PARTDIM}".err | cut -d ' ' -f2 | cut -d')' -f1 \
    > "${F_POS_PARTDIM}".ext
  verify_equal_cardinality "${F_POS_PARTDIM}".err "${F_POS_PARTDIM}".ext

  log_echo "'${SCRIPT_RESET_PATHS}.ext.sh' build and exec ..."
  echo "rm -f '${SCRIPT_RESET_PATHS}.ext.sh.log' 2>/dev/null" > "${SCRIPT_RESET_PATHS}.ext.sh"

  while read pos_at_partdim ; do
cat >> "${SCRIPT_RESET_PATHS}.ext.sh" <<EOF
populateArray -array '${ARR_DOM_PATH}' -set '${PDIM}:${pos_at_partdim}' -type string -value '${DEST_DIR}/${MASTER_DOMAIN}' >> '${SCRIPT_RESET_PATHS}.ext.sh.log' 2>&1 || exit ${EXIT_STATUS_ERR:-1}
EOF
  done < "${F_POS_PARTDIM}".ext

  exec_script "${SCRIPT_RESET_PATHS}.ext.sh"

}

repoint_reset_indices () {

  # TODO check portability:
  printArray -array "${ARR_DOM_INDX}" | grep "${RE_LSD}" | \
    cut -d '(' -f2 | \
    cut -d ')' -f1 | \
    tr -s ' ' '\n' | \
    grep -v '^$' \
    > "${WKDIR}"/indx_to_subdomains

  log_echo "'${SCRIPT_RESET_INDICES}' build and exec ..."
  echo "rm -f '${SCRIPT_RESET_INDICES}.log' 2>/dev/null" > "${SCRIPT_RESET_INDICES}"

  while read indx_to_subdomains ; do
cat >> "${SCRIPT_RESET_INDICES}" <<EOF
populateArray -array '${ARR_DOM_INDX}' -set 'INDX:${indx_to_subdomains}' -type string -value '${DEST_DIR}/${MASTER_DOMAIN}' >> '${SCRIPT_RESET_INDICES}.log' 2>&1 || exit ${EXIT_STATUS_ERR:-1}
EOF
  done < "${WKDIR}"/indx_to_subdomains

  exec_script "${SCRIPT_RESET_INDICES}"

}

repoint_domains_to_master () {

  log_echo "Assigning MASTER_PATH='${DEST_DIR}/${MASTER_DOMAIN}' ..."

  # This is a rather simple execution and logging so no script is generated:
  > "${WKDIR}/repoint_domains_to_master.log"
  for i in ${LOCAL_DOMAIN_LIST} ; do
    echo "populateArray -array '${DEST_DIR_LOCALS}/${i}/data/admin.domain_properties' -set 'INFO:MASTER_PATH' -type string -value '${DEST_DIR}/${MASTER_DOMAIN}'" \
      >> "${WKDIR}/repoint_domains_to_master.log"
    populateArray -array "${DEST_DIR_LOCALS}/${i}/data/admin.domain_properties" -set "INFO:MASTER_PATH" -type string -value "${DEST_DIR}/${MASTER_DOMAIN}" \
      >> "${WKDIR}/repoint_domains_to_master.log" 2>&1
    verify_exec "$?" "Error. See '${WKDIR}/repoint_domains_to_master.log'."
  done

}

# Reconfig to delete 'out-of-scope' partitions:
# REMARK: Master remains as per the 'point-to-master, remove-only-wanted' technique.
remove_via_reconfig () {

  xargs_threshold=400
  pos_to_rm_count="$(wc -l "${F_POS_RM}"|awk '{print $1;}')"
  pos_to_rm_exec_count="$((pos_to_rm_count/xargs_threshold+1))"

  log_echo "reconfig will run ${pos_to_rm_exec_count} times to remove ${pos_to_rm_count} positions (${xargs_threshold} each time)."

  cd "${DEST_DIR}/${MASTER_DOMAIN}"
  reconfig_number=0
  > "${F_LOG_RECONFIG}"
  cat "${F_POS_RM}" | xargs -n ${xargs_threshold} echo | sed -e 's/  */,/g' -e 's/^,//' -e 's/,$//' | \
  while read args ; do
    reconfig_number=$((reconfig_number+1))
    log_echo "reconfig # ${reconfig_number} of ${pos_to_rm_exec_count} ..."
    log_echo -q "reconfigGlobalDomainPartitions -d '${DEST_DIR}/${MASTER_DOMAIN}' -remove '${args}' >> '${F_LOG_RECONFIG}' 2>&1"
    reconfigGlobalDomainPartitions -d "${DEST_DIR}/${MASTER_DOMAIN}" -remove "${args}" >> "${F_LOG_RECONFIG}" 2>&1
    verify_exec "$?" "reconfig failed. See '${F_LOG_RECONFIG}'."
  done
  cd > /dev/null 2>&1

}

repoint_paths_set_ext_ldom () {

  printArray -array "${ARR_PDIM_EXT}" | \
    grep -f "${F_POS_REPOINT}".err | \
    sed -e 's/^(EXTNAME //' | tr -d ') "' \
    > "${F_POS_REPOINT}".ext_to_pos

  # Substitutes pos in F_POS_REPOINT_LDOM.err by extname for pos
  # as in F_POS_REPOINT.ext_to_pos > F_POS_REPOINT_LDOM.ext file:
  while IFS=':' read pos ldom ; do
    echo "$(grep -F "${pos}" "${F_POS_REPOINT}".ext_to_pos | cut -d':' -f1):${ldom}"
  done < "${F_POS_REPOINT_LDOM}".err > "${F_POS_REPOINT_LDOM}".ext

  verify_equal_cardinality "${F_POS_REPOINT}".err "${F_POS_REPOINT_LDOM}".ext

}

repoint_paths () {

  log_echo "'${SCRIPT_SET_PATHS}' build and exec ..."
  echo "rm -f '${SCRIPT_SET_PATHS}.log' 2>/dev/null" > "${SCRIPT_SET_PATHS}"

  rm -f "${F_POS_REPOINT}".err "${F_POS_REPOINT_LDOM}".err 2> /dev/null

  while IFS=':' read pos ldom ; do
cat >> "${SCRIPT_SET_PATHS}" <<EOF
populateArray -array '${ARR_DOM_PATH}' -set '${PDIM}:${PDIM}${pos}' -type string -value '${DEST_DIR_LOCALS}/${ldom}' >> '${SCRIPT_SET_PATHS}.log' 2>&1
if [ "\$?" -ne 0 ] ; then
  echo '${pos}' >> '${F_POS_REPOINT}'.err
  echo '${pos}:${ldom}' >> '${F_POS_REPOINT_LDOM}'.err
fi
EOF
  done < "${F_POS_REPOINT_LDOM}"

  exec_script_tolerant "${SCRIPT_SET_PATHS}"

  # Ext cells remaining. Errors are caused by ext_PDIM
  #  cells whose external names were in 'populateArray -set ...',
  #  so we extract this ext positions and make a new pos:ldom file,
  #  "${F_POS_REPOINT_LDOM}".ext:
  repoint_paths_set_ext_ldom

  log_echo "'${SCRIPT_SET_PATHS}.ext.sh' build and exec ..."
  echo "rm -f '${SCRIPT_SET_PATHS}.ext.sh.log' 2>/dev/null" > "${SCRIPT_SET_PATHS}.ext.sh"

  while IFS=':' read pos ldom ; do
cat >> "${SCRIPT_SET_PATHS}.ext.sh" <<EOF
populateArray -array '${ARR_DOM_PATH}' -set '${PDIM}:${pos}' -type string -value '${DEST_DIR_LOCALS}/${ldom}' >> '${SCRIPT_SET_PATHS}.ext.sh.log' 2>&1 || exit ${EXIT_STATUS_ERR:-1}
EOF
  done < "${F_POS_REPOINT_LDOM}".ext

  exec_script "${SCRIPT_SET_PATHS}.ext.sh"

}

# Writes {domain index}:{domain path} pairs to "${F_INDX_LDOM}":
repoint_indices () {

  log_echo 'Building hash file of indx:ldom pairs ...'
  domaininfo -d "${DEST_DIR}/${MASTER_DOMAIN}" -shellsubdomains | \
    awk '{print NR ":" $0;}' \
    > "${F_INDX_LDOM}"

  log_echo "'${SCRIPT_SET_INDICES}' build and exec ..."
  echo "rm -f '${SCRIPT_SET_INDICES}.log' 2>/dev/null" > "${SCRIPT_SET_INDICES}"

  while IFS=':' read indx ldom ; do
cat >> "${SCRIPT_SET_INDICES}" <<EOF
populateArray -array '${ARR_DOM_INDX}' -set 'INDX:${indx}' -type string -value '${ldom}' >> '${SCRIPT_SET_INDICES}.log' 2>&1 || exit ${EXIT_STATUS_ERR:-1}
EOF
  done < "${F_INDX_LDOM}"

  exec_script "${SCRIPT_SET_INDICES}"

}

repoint_shadows_to_master () {

  log_echo "Repointing data/shadow.db databases to master: '${DEST_DIR}/${MASTER_DOMAIN}/data/shadow'."

  for i in ${LOCAL_DOMAIN_LIST} ; do
    log_echo -q "echo '${DEST_DIR}/${MASTER_DOMAIN}/data/shadow' > '${DEST_DIR_LOCALS}/${i}/data/shadow.db/r_mastershadowpath.cfg'"
    echo "${DEST_DIR}/${MASTER_DOMAIN}/data/shadow" > "${DEST_DIR_LOCALS}/${i}/data/shadow.db/r_mastershadowpath.cfg"
  done

}

clean_files () {
  rm -f \
    "${F_INDX_LDOM}" \
    "${F_LSD}"* \
    "${F_POS_PARTDIM}"* \
    "${F_POS_REPOINT}"* \
    "${F_POS_REPOINT_LDOM}"* \
    "${F_POS_RM}" \
    "${SCRIPT_RESET_INDICES}"* \
    "${SCRIPT_RESET_PATHS}"* \
    "${SCRIPT_SET_INDICES}"* \
    "${SCRIPT_SET_PATHS}"*
}

# Main:

init_workspace
init_log
repoint_prepare_env
repoint_prepare
repoint_reset_paths
repoint_reset_indices
repoint_domains_to_master
remove_via_reconfig
repoint_paths
repoint_indices
repoint_shadows_to_master

#clean_files

