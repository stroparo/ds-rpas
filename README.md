DS - Daily Shells Library - RPAS variant
========================================

Author: Cristian Stroparo

License: To be defined; meanwhile this is a closed license solely at the author's discretion (Cristian Stroparo).

License for the base DS library: see that project's README.md file at https://github.com/stroparo/ds

Remark
------
This library version is developed and maintained 100% in the author's free time but you can contribute by forking this repository and making a pull request.

---

Installation
------------

Overlay these files over a [ds installation](https://github.com/stroparo/ds).

---

Brief Descriptions & Examples
-----------------------------

- Daemon aliases

```
ddping
ddps
ddstop
ddstopps
```

- Export hierarchy:

```
exph {hierarchy-name}
```

- Export measure:

```
expm {measure} [options]
```

- Export measure at specific intx:

```
expmati {measure} {intx} [options]
```

- Iterate on subdomains, executing the given command:

```
eachsubdo echo
eachsubdo mace -run -expression '...' -d
```

- Listing commands:

```
lsdomains
lsgroups
lslevels
lsusers
```

- Reindex Domain -analyze:

```
rx
```

- Reindex Domain -force:

```
rxforce     # -dimSpec 'rule'
rxforceall  # -dimSpec 'rule,expr,meas,wb,wbq'
```

- Rule / Expression Search ("rg" is an abbrevation for "rgrep"):

```
rg    {egrep-regexp}
rga   {egrep-regexp-LHS} # rg = rgrep; a = assignment
rgr   {egrep-regexp-RHS} # rg = rgrep; r = read
```

- Stats commands:

```
mpop {measure}
mstats {measure}
```

---

HowTo - clonemeas.sh
--------------------

General idea of this script is to obtain attributes (properties) of a measure via
printMeasure -specs and create a regmeasure command that recreates that measure.

Known bug when the attribute value is negative:
Solution: for range, use '[-1:1]', for purgeage, use ' -1', for navalue, use '"-1"'

Examples:

To print the regmeasure command in one line:

    clonemeas.sh <DOMAIN> <MEASURE>

To print the regmeasure command in several lines:

    clonemeas.sh <DOMAIN> <MEASURE> " \n"

To print the regmeasure command in several lines, each of which with a \ (backslash) before the EOL:

    clonemeas.sh <DOMAIN> <MEASURE> " \\\\\n"

To print the regmeasure command so that it is convertible later on to an XML (Tools):

    clonemeas.sh <DOMAIN> <MEASURE> "\n" | egrep -v "^-[a-z]* ''" | tr -d '"' | tr -d "'" | sed -e 's/-range \[\(.*\)\]/-range \1/'

---

Syntax for Some Functions
-------------------------

    ary {hmaint-arrayname-with-no-extension}

    findwblogs [yyyymmdd]

    minfo {measure}

    pmd {measure} [domain_override] [printMeasure arguments/commands]

    pmsubs {measure} [printMeasure arguments/commands]


---

Call Examples
-------------

    clonemeas.sh "${DOM}" measure " \\\\\n"

    expmati cystklr wk__ -range w201425:w201432

    mkusers.sh -g rpasadmins Password2 jose pedro

    mkusers.sh -a Password2 jose pedro

    pmd {measure} -allPopulatedCells -cellsPerRow 1

