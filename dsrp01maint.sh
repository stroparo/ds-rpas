#!/usr/bin/env bash

# DS-RPAS - Daily Shells Library for RPAS
# License:
#  See README.md document in projects page at
#  https://github.com/stroparo/ds-rpas

# ##############################################################################
# RPAS maintenance functions

# Oneliners:
rx          () { reindexDomain -d "${DOM}" -dimSpec 'rule' -analyze ; }
rxforce     () { reindexDomain -d "${DOM}" -dimSpec 'rule' -force ; }
rxforceall  () { reindexDomain -d "${DOM}" -dimSpec 'rule,expr,meas,wb,wbq' -force ; }

# Function arygz - gzips all arrays greater than 1MB in place in the domain.
# Syntax: [-c compression-level] {domain}
unset arygz
arygz () {
    typeset domain
    typeset dulist
    typeset gziplevel=1
    typeset logdir="${DS_ENV_LOG}"
    typeset maxprocs=16
    typeset paracmd
    typeset pname='arygz'

    # Options:
    OPTIND=1
    while getopts ':c:l:p:' opt ; do
        case "${opt}" in
        c) gziplevel="${OPTARG}";;
        l) logdir="${OPTARG}";;
        p) maxprocs="${OPTARG}";;
        esac
    done
    shift $((OPTIND - 1)) ; OPTIND=1

    domain="${1}"
    isdomain "${domain}" || return 10
    setlogdir "${logdir}" || return 11

    # Gzip compression:
    if ! [[ $gziplevel = [1-9] ]] ; then
        elog -f -n "${pname}" "'$gziplevel' not a valid gzip compression level (must be 1..9)."
        return 20
    fi
    elog -n "${pname}" "Compression level is ${gziplevel}"

    # File list
    elog -n "${pname}" "Compressing arrays.."
    elog -n "${pname}" "Initial delay may ocurr whilst sorting file list by size (desc).."
    dulist="$(lsary "${domain}" | dugt1desc | printawk -F. -p '$1 >= 5' 0)"

    paracmd="gzip -${gziplevel:-1} {}"

    elog "Entries to be processed 500+ MB: $(echo "${dulist}" | printawk -F. -p '$1 >= 500' 0 | wc -l)"
    paralleljobs -n 2 -l "${logdir}" -p ${maxprocs} "${paracmd}" <<EOF
$(echo "${dulist}" | printawk -F. -p '$1 >= 500' 0 | dufile)
__HALT__
EOF
    elog "Entries to be processed 128..500MB: $(echo "${dulist}" | printawk -F. -p '$1 >= 128 && $1 < 500' 0 | wc -l)"
    paralleljobs -n 6 -l "${logdir}" -p ${maxprocs} "${paracmd}" <<EOF
$(echo "${dulist}" | printawk -F. -p '$1 >= 128 && $1 < 500' 0 | dufile)
__HALT__
EOF
    elog "Entries to be processed 64..128MB: $(echo "${dulist}" | printawk -F. -p '$1 >= 64 && $1 < 128' 0 | wc -l)"
    paralleljobs -n 28 -l "${logdir}" -p ${maxprocs} "${paracmd}" <<EOF
$(echo "${dulist}" | printawk -F. -p '$1 >= 64 && $1 < 128' 0 | dufile)
__HALT__
EOF
    elog "Entries to be processed 5..64MB: $(echo "${dulist}" | printawk -F. -p '$1 < 64' 0 | wc -l)"
    paralleljobs -n 64 -l "${logdir}" -p ${maxprocs} "${paracmd}" <<EOF
$(echo "${dulist}" | printawk -F. -p '$1 < 64' 0 | dufile)
__HALT__
EOF
    # elog 'done debugging'
    # return
}

# Function arygunz - gunzips all arrays in the DOM master domain.
# Syntax: {domain}
unset arygunz
arygunz () {
    typeset domain
    typeset dulist
    typeset logdir="${DS_ENV_LOG}"
    typeset maxprocs=16
    typeset paracmd="gunzip {}"

    # Options:
    OPTIND=1
    while getopts ':l:p:' opt ; do
        case "${opt}" in
        l) logdir="${OPTARG}";;
        p) maxprocs="${OPTARG}";;
        esac
    done
    shift $((OPTIND - 1)) ; OPTIND=1

    domain="${1}"
    isdomain "${domain}" || return 10
    setlogdir "${logdir}" || return 11

    elog -n "${pname}" "Decompressing arrays.."
    elog -n "${pname}" "Initial delay may ocurr whilst sorting file list by size (desc).."
    dulist="$(lsarygz "${domain}" | dudesc)"

    elog "Entries to be processed 5- MB: $(echo "${dulist}" | printawk -F. -p '$1 < 5' 0 | wc -l)"
    paralleljobs -n 20 -l "${logdir}" -p ${maxprocs} "${paracmd}" <<EOF
$(echo "${dulist}" | printawk -F. -p '$1 < 5' 0 | dufile)
__HALT__
EOF
    elog "Entries to be processed 5..50MB: $(echo "${dulist}" | printawk -F. -p '$1 >= 5 && $1 < 50' 0 | wc -l)"
    paralleljobs -n 8 -l "${logdir}" -p ${maxprocs} "${paracmd}" <<EOF
$(echo "${dulist}" | printawk -F. -p '$1 >= 5 && $1 < 50' 0 | dufile)
__HALT__
EOF
    elog "Entries to be processed 50..200MB: $(echo "${dulist}" | printawk -F. -p '$1 >= 50 && $1 < 200' 0 | wc -l)"
    paralleljobs -n 4 -l "${logdir}" -p ${maxprocs} "${paracmd}" <<EOF
$(echo "${dulist}" | printawk -F. -p '$1 >= 50 && $1 < 200' 0 | dufile)
__HALT__
EOF
    elog "Entries to be processed 200+MB: $(echo "${dulist}" | printawk -F. -p '$1 >= 200' 0 | wc -l)"
    paralleljobs -n 2 -l "${logdir}" -p ${maxprocs} "${paracmd}" <<EOF
$(echo "${dulist}" | printawk -F. -p '$1 >= 200' 0 | dufile)
__HALT__
EOF
}

# Function bkdomsplit - backup domain splitting into 2000m size files, tar gzip format.
# Remark: Use bkdomsplitrestore function to restore a backup made with this function.
# Syntax: {DESTINATION-DIR} {FILENAME-SPLIT-PREFIX}
# Example:
# bkdomsplit /staging/rdf/tmp prd-$(date '+%y%m%d-%OH%OM%OS').tgz."
unset bkdomsplit
bkdomsplit () {
    typeset pname=bkdomsplit

    if [[ -d "${DOM}/repos" ]] ; then
        elog -f -n "${pname}" "DOM variable (${DOM}) must point to a master domain."
        return
    fi
    export domainbasename="$(basename "${DOM}")"
    export dest_dir="${1}"
    export splitprefix="${2}"

    nohup bash -c '
        tar -cf - -C "${DOMPARENT}" "${domainbasename}" | \
        gzip -1c - | \
        split -b 2000m - "${dest_dir}/${domainbasename}-${splitprefix}"' \
    > "${dest_dir}/${domainbasename}-${splitprefix%.}.nohup.out" 2>&1 \
    &
}

# Function bkdomsplitrestore - restores a domain backup.
# Remark: preferred use with a backup created by the bkdomsplit function.
# Syntax: {TGZ-FILENAME-PREFIX}
unset bkdomsplitrestore
bkdomsplitrestore () {
    typeset pname=bkdomsplitrestore

    export path_prefix="${1}"
    export dest_dir="${DOMPARENT}"
    export base_prefix="$(basename "${path_prefix}")"

    elog -n "${pname}" "Starting.."
    domarchive || return 1

    elog -n "${pname}" "Extracting.. (log at '${dest_dir}/${base_prefix%.}.nohup.out')"

    nohup bash -c '
        cat "${path_prefix}"* | \
        gunzip -c - | \
        tar -xf - -C "${dest_dir}"' \
    > "${dest_dir}/${base_prefix%.}.restore.out" 2>&1 \
    &
}

# Function domarchive - Move the domain to a timestamped one.
unset domarchive
domarchive () {
    typeset pname=domarchive
    typeset mfatal="Could not move domain DOM='${DOM}' out of the way."
    typeset mnodomain="Nothing done because '${DOM}' is not a domain (does not have repos subdir)."
    typeset ts="$(date '+%y%m%d-%OH%OM')"

    if [[ -d "${DOM}/repos" ]] ; then
        elog -n "${pname}" "Moving '${DOM}' to '${DOM}-${ts}'.."

        if ! mv "${DOM}" "${DOM}-${ts}" ; then
            elog -f -n "${pname}" "${mfatal}"
            return 1
        fi
    else
        elog -w -n "${pname}" "${mnodomain}"
    fi

    # Reinforce move check, and if still not ok then abort:
    if [[ -d "${DOM}/repos" ]] ; then
        elog -f -n "${pname}" "${mfatal}"
        return 1
    fi
}

# Function domclean - Cleans domain, workbooks, input files (except hier *.dat).
# Options:
#   -p maxprocs[=16]
# Syntax: [-p {maxprocs}] {domain}
unset domclean
domclean () {

    typeset pname=domclean

    typeset domaindir
    typeset maxprocs=16
    typeset onlyclean=false
    typeset wbonly=false

    # Options:
    OPTIND=1
    while getopts ':p:w' opt ; do
        case "${opt}" in
        p) maxprocs="${OPTARG}" ;;
        w) wbonly=true ;;
        esac
    done
    shift $((OPTIND - 1)) ; OPTIND=1

    domaindir="$(cd "${1}" ; echo "$PWD")"
    isdomain "${domaindir}" || return 10

    elog -n "${pname}" "Cleaning domain '${domaindir}'.."

    if ! ${wbonly} ; then
        elog -n "${pname}" "Purging @config's files.."
        rm -f "${domaindir}/config/"*.tar.gz "${domaindir}"/*.log

        elog -n "${pname}" "Purging @input's except .dat hierarchy files.."
        find "${domaindir}"/input "${domaindir}"/*/input -type f ! -name '*.dat.*' -exec rm {} \;
    fi

    elog -n "${pname}" "Purging workbooks concurrently.."
    paralleljobs -p ${maxprocs} "wbmgr -d {} -remove -all" <<EOF
$(lsdomains -a -r "${domaindir}")
EOF
    wait
}

# Function domunpack - unpack / restore a domain backup. This opposes bkdomsplitrestore in
#  that it does not accept prefix logic to process a splitted backup.
#  It uses $DOMPARENT/*gz and additional arguments as the selection menu.
#
# Deps: bash, ds.unarchive()
#
# Example:
# domunpack /bkp*/*gz
unset domunpack
domunpack () {
    typeset pname=domunpack

    elog -n "${pname}" "Starting.."
    domarchive || return 1

    select filename in "$@" "${DOMPARENT}"/*gz ; do
        if [ ! -f "${filename}" ] ; then
            elog -f -n "${pname}" "Not a valid filename."
            return 1
        fi

        elog -n "${pname}" "Generating temp script '${HOME}/${pname}.sh'"

        cat > "${HOME}/${pname}.sh" <<EOF
#!/usr/bin/env bash
. ~/.ds/ds.sh && unarchive -v -o '${DOMPARENT}' '${filename}'
res=\$?
echo "\$res"
exit "\$res"
EOF
        elog -n "${pname}" "Launching temp script '${HOME}/${pname}.sh'.."
        elog -n "${pname}" "The log will be in '${DOMPARENT}/$(basename "${filename}").unpack.out'"

        nohup bash "${HOME}/${pname}.sh" \
        > "${DOMPARENT}/$(basename "${filename}").unpack.out" \
        2>&1 \
        &

        break
    done
}

# Function genreconfigxml
# TODO data agnostic
# TODO syntax
unset genreconfigxml
genreconfigxml () {

    typeset paths_positions="${1}"
    # TODO env check function call

    cp "${DOM}/config/reconfigpartdim.xml" "${DOM}/config/reconfigpartdim.xml.$(date '+%Y%m%d-%OH%OM%OS')" 2> /dev/null
    
    cat > "${DOM}/config/reconfigpartdim.xml" <<'EOF'
<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<rpas>
    <command name="add">
EOF

    while read domain position ; do
        cat >> "${DOM}/config/reconfigpartdim.xml" <<EOF
        <subdomain>
            <subpath>/u01/rdf/environments/rdf_prd_code/domains/CEA_RDF_CODE/ldom${domain}</subpath>
            <subpositions>${position}</subpositions>
        </subdomain>
EOF
    done <<EOF
${paths_positions}
EOF

    cat >> "${DOM}/config/reconfigpartdim.xml" <<'EOF'
    </command>
</rpas>
EOF
}

# Function: hiercsv2pipe
# Desc:   Parses an RPAS hierarchy file and converts from csv to pipes.
#          This is effective, as commas inside labels "...,..." are kept.
# Deps:   awk
# Output: ?
# Retval: standard.
hiercsv2pipe () {

    ckenv 1 1 "$@"

    typeset hier_file="${1}"
    typeset usage='{hierarchy-file}'

    [ -f "${1}" ] || return

    awk '
     {
        for (i=1; i <= length($0); i++) {
            this_char = substr($0,i,1);
            this_two = substr($0,i,2);

            (!inside && (this_char ~ /,/) && printf("|")) \
                || (!inside && (this_char ~ /"/) && inside = 1) \
                || (inside && (this_two ~ /""/) && printf("\"")) \
                || (inside && (this_two ~ /",/ && inside = 0);
                || printf("%c", this_char);
        };
        printf("\n");
     }
     ' "${hier_file}"

}

unset mktaskflow
mktaskflow () {

    ckenv 0 0 "$@"

    if [ -d "${DOM}" -a -d "${FUSION}" ] ; then
        rm -rf "${FUSION}"/config/MultiSolution
        mkdir -p "${FUSION}"/config/MultiSolution/resources
        cp -v "${DOM}"/fusionClient/taskflow.xml "${FUSION}"/config/MultiSolution/Taskflow_MultiSolution.xml
        cp -v "${DOM}"/fusionClient/taskflowBundle.properties "${FUSION}"/config/MultiSolution/resources/MultiSolutionBundle.properties
    fi

    find "${FUSION}"/config -type f \( -name 'Taskflow*.xml' -o -name '*Bundle.properties' \) -ls

}

# Function mkusers - Makes users according to options.
# Syntax TODO (use -h option for now)
unset mkusers
mkusers () {
    typeset admin group pw
    typeset pname='mkusers'
    typeset usage='[-a] [-g group] {password} {user1} [user2 [user3 ...]]
    -a yields -admin to the RPAS command.'

    ckenv 2 127 "$@"

    # Options:
    OPTIND=1
    while getopts ':ag:' opt ; do
        case "${opt}" in
        a) admin=true ;;
        g) group="${OPTARG}" ;;
        h|*) elog -n "${pname}" "${usage}" ; return 0 ;;
        esac
    done
    shift $((OPTIND - 1)) ; OPTIND=1

    # Defaults:
    : ${group:=rpasadmins}

    # Password is the 1st argument:
    pw="${1}"
    shift

    # Create the group, if it is there, an (expected) error will be printed:
    usermgr -d "${DOM}" -addGroup "${group}" -label "${group}" \
    && elog -n "${pname}" "Created ${group} group."

    # Create the user itself:
    for user in "$@" ; do
      usermgr -d "${DOM}" -add "${user}" -label "${user}" -group "${group}" ${admin:+-admin} <<EOF
${pw}
${pw}
EOF
    done

}

# The following routine requires DS' routine(s) are available:
unset pgrpas
pgrpas () {
    echo 'Processes for RPAS:'
    pgr "[.]ksh|[.]sh|batch|DomainDaemon|exportMeasure|mace|loadHier|loadmeasure|RpasDbServer${1:+|${1}}"
}

# Function rmlocks - removes all locks inside the DOM master domain directory.
unset rmlocks
rmlocks () {
    ckenv 0 0 "$@"
    find "${DOM}" -type f -name '*.db.lck' -exec rm -f {} \;
}

# The following routine requires DS' routine(s) are available:
unset rmon
rmon () {
    loop -d 10 pgrpas "${1}"
}

# Function: zipoutputlogs
# Desc:   Compresses log directories found inside the master domain's output directory.
# Deps:   fgrep, ls, tar, rm
# Remark: Directories are removed only upon tar|gz success.
# Usage:  no arguments needed
# Output:
#   Generating <...>.tar.gz
#   Generating <...>.tar.gz
#   ...
# Retval: standard.
unset zipoutputlogs
zipoutputlogs () {
    typeset logdir="${DS_ENV_LOG}"
    typeset maxprocs=16
    typeset paracmd="gzip -1 {}"

    # Options:
    OPTIND=1
    while getopts ':l:p:' opt ; do
        case "${opt}" in
        l) logdir="${OPTARG}";;
        p) maxprocs="${OPTARG}";;
        esac
    done
    shift $((OPTIND - 1)) ; OPTIND=1

    paralleljobs -l "${logdir}" -p ${maxprocs} "${paracmd}" <<EOF
$(find "${DOM}/output" -type f ! -name '*gz' -name '*2[0-9][0-9][0-9]*')
EOF
}

